#include "p16F628A.inc"

; CONFIG
; __config 0xFFF0
  __CONFIG _FOSC_INTOSCIO & _WDTE_OFF & _PWRTE_ON & _MCLRE_ON & _BOREN_ON & _LVP_OFF & _CPD_OFF & _CP_OFF

#define DISP_TMP 0x20
#define DISP_A 0x21
#define DISP_B 0x22
#define PULSE_LEN 0x23
  
RES_VECT CODE 0x0000
    GOTO START

; TODO ADD INTERRUPTS HERE IF USED

MAIN_PROG CODE

DISP_CLK
    BCF PORTA, 0
    BTFSC DISP_TMP, 7
    BSF PORTA, 0
    BSF PORTA, 1
    ; TODO add delay if we'd happen to speed things up
    BCF PORTA, 1
    RLF DISP_TMP, F
    RETURN

;  222
; 1   3
;  000
; 4   6
;  555 7

DISP_DIGIT_LUT
    ADDWF PCL
    RETLW ~0x7E ; 0
    RETLW ~0x48 ; 1
    RETLW ~0x3D ; 2
    RETLW ~0x6D ; 3
    RETLW ~0x4B ; 4
    RETLW ~0x67 ; 5
    RETLW ~0x77 ; 6
    RETLW ~0x4C ; 7
    RETLW ~0x7F ; 8
    RETLW ~0x6F ; 9

DISP_DIGIT
    CALL DISP_DIGIT_LUT
    MOVWF DISP_TMP
    CALL DISP_CLK
    CALL DISP_CLK
    CALL DISP_CLK
    CALL DISP_CLK
    CALL DISP_CLK
    CALL DISP_CLK
    CALL DISP_CLK
    CALL DISP_CLK
    RETURN

DISP_NUM
    MOVWF DISP_TMP
    ; from www.piclist.com/techref/microchip/math/radix/b2bhp-8b3d.htm
    CLRF DISP_A
    SWAPF DISP_TMP, W ; swap the nibbles
    ADDWF DISP_TMP, W ; so we can add the upper to the lower
    ANDLW B'00001111' ; lose the upper nibble (W is in BCD from now on)
    SKPNDC ; if we carried a one (upper + lower > 16)
    ADDLW 0x16 ; add 16 (the place value) (1s + 16 * 10s)
    SKPNDC ; did that cause a carry from the 1's place?
    ADDLW 0x06 ; if so, add the missing 6 (carry is only worth 10)
    ADDLW 0x06 ; fix max digit value by adding 6
    SKPDC ; if was greater than 9, DC will be set
    ADDLW -0x06 ; if if it wasn't, get rid of that extra 6
    BTFSC DISP_TMP, 4 ; 16's place
    ADDLW 0x16 - 1 + 0x6 ; add 16 - 1 and check for digit carry
    SKPDC
    ADDLW -0x06 ; if nothing carried, get rid of that 6
    BTFSC DISP_TMP, 5 ; 32nd's place
    ADDLW 0x30 ; add 32 - 2
    BTFSC DISP_TMP, 6 ; 64th's place
    ADDLW 0x60 ; add 64 - 4
    BTFSC DISP_TMP, 7 ; 128th's place
    ADDLW 0x20 ; add 128 - 8 % 100
    ADDLW 0x60 ; has the 10's place overflowed?
    RLF DISP_A, F ; pop carry in hundreds' LSB
    BTFSS DISP_A, 0 ; if it hasn't
    ADDLW -0x60 ; get rid of that extra 60
    MOVWF DISP_B ; save result
    BTFSC DISP_TMP, 7 ; remeber adding 28 - 8 for 128?
    INCF DISP_A, F ; add the missing 100 if bit 7 is set
    ; show the digits
    MOVF DISP_A, W
    CALL DISP_DIGIT
    SWAPF DISP_B, W
    ANDLW B'00001111'
    CALL DISP_DIGIT
    MOVF DISP_B, W
    ANDLW B'00001111'
    CALL DISP_DIGIT
    RETURN

NAP
    clrf 0x140
lo0
    clrf 0x141
    decfsz 0x141
    goto $ - 1
    decfsz 0x140
    goto lo0
    RETURN

START
    BANKSEL PORTA
    CLRF PORTA
    BANKSEL TRISA
    CLRF TRISA
    BANKSEL CMCON
    MOVLW 0x07
    MOVWF CMCON
    
    BANKSEL PORTA

    CLRF PULSE_LEN
lop
    MOVF PULSE_LEN, W
    CALL DISP_NUM
    INCF PULSE_LEN, F

    call NAP
;    call NAP
;    call NAP
    
    GOTO lop

    END

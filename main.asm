#include "p16F628A.inc"

; TODO
; - bigger heatsink or lower voltage xformer on psu
; - listen to pedal instead of pulsing all the time
; - remember not to put call to PULSE inside ISR !

; CONFIG
; __config 0xFFF0
  __CONFIG _FOSC_INTOSCIO & _WDTE_OFF & _PWRTE_ON & _MCLRE_ON & _BOREN_ON & _LVP_OFF & _CPD_OFF & _CP_OFF

#define D0 0x70
#define D1 0x71
#define D2 0x72
#define D3 0x73
#define NUM_HI 0x74
#define NUM_LO 0x75
#define SAVE_W 0x76
#define SAVE_STATUS 0x77
#define PORTB_SAVE 0x78
#define ROTARY_STATE 0x79
#define TMP 0x7A
#define PULSE_COUNT_HI 0x7B
#define PULSE_COUNT_LO 0x7C
#define PULSE_TMP 0x7D
#define STATE 0x7E ; bit 0 means pulse is or should be going

RES_VECT CODE 0x0000
    GOTO START

INT_VECT CODE 0x0004
    ; save state and go to bank 0
    MOVWF SAVE_W ; save W
    SWAPF STATUS, W ; SAVE STATUS (swapped)
    MOVWF SAVE_STATUS
    BANKSEL 0
    ; check if INTF set (pedal pressed)
    BTFSS INTCON, INTF
    GOTO ENDINTA
    BCF INTCON, INTF
    BSF STATE, 0
ENDINTA
    ; check if portb changed
    BTFSS INTCON, RBIF
    GOTO ENDINTB
    ; workaround for PORTB latch bug
    ; http://www.microchip.com/forums/m294169.aspx
    MOVF PORTB, W ; this could read bullshit, apparently
    BCF INTCON, RBIF
    MOVF PORTB, W
    MOVWF PORTB_SAVE
    ; now we shift in the two bits from rotary encoder
    MOVLW 0x0F
    ANDWF ROTARY_STATE, F
    BCF STATUS, C
    RLF ROTARY_STATE, F
    RLF ROTARY_STATE, F
    SWAPF PORTB_SAVE, W ; strip relevant bits from portb
    ANDLW 3
    IORWF ROTARY_STATE, F
    ; check increment
    MOVF ROTARY_STATE, W
    XORLW ~0xED
    BTFSS STATUS, Z
    GOTO $ + 3
    CALL INC_NUM
    CALL DISP_NUM
    ; check decrement
    MOVF ROTARY_STATE, W
    XORLW ~0xDE
    BTFSS STATUS, Z
    GOTO $ + 3
    CALL DEC_NUM
    CALL DISP_NUM
    ; restore state
ENDINTB
    SWAPF SAVE_STATUS, W ; restore status
    MOVWF STATUS
    SWAPF SAVE_W, F ; restore W without fucking status
    SWAPF SAVE_W, W
    RETFIE

MAIN_PROG CODE

DISP_DIGIT_LUT
    ADDWF PCL, F
    RETLW ~0x7E ; 0
    RETLW ~0x12 ; 1
    RETLW ~0xBC ; 2
    RETLW ~0xB6 ; 3
    RETLW ~0xD2 ; 4
    RETLW ~0xE6 ; 5
    RETLW ~0xEE ; 6
    RETLW ~0x32 ; 7
    RETLW ~0xFE ; 8
    RETLW ~0xF6 ; 9
    
DISP_DIGIT
    CALL DISP_DIGIT_LUT
    MOVWF TXREG
    BANKSEL TXSTA
    BTFSS TXSTA, TRMT
    GOTO $ - 1
    BANKSEL 0
    RETURN

BINTODEC
    CLRF D2
    SWAPF NUM_LO, W ; n >>= 4;
    ANDLW 0xF
    MOVWF D0
    SWAPF NUM_HI, W
    IORWF D0, F
    MOVLW 0x18 ; if (n >= 0x28) n += 0x18;
    ADDWF D0, F
    BTFSS D0, 6
    SUBWF D0, F
    MOVLW 0xC ; if ((n & 0x3C) >= 0x14) n += 0xC;
    ADDWF D0, F
    BTFSS D0, 5
    SUBWF D0, F
    MOVLW 0x6 ; if ((n & 0x1E) >= 0xA) n += 0x6;
    ADDWF D0, F
    BTFSS D0, 4
    SUBWF D0, F
    MOVLW 0x30 ; if ((n & 0xF0) >= 0x50) n += 0x30;
    ADDWF D0, F
    BTFSS D0, 7
    SUBWF D0, F
    MOVLW 0x3 ; if ((n & 0xF) >= 0x5) n += 0x3;
    ADDWF D0, F
    BTFSS D0, 3
    SUBWF D0, F
    MOVLW 0x18 ; if ((n & 0x78) >= 0x28) n += 0x18;
    ADDWF D0, F
    BTFSS D0, 6
    SUBWF D0, F
    SWAPF NUM_LO, W
    MOVWF TMP
    RLF TMP, F
    RLF D0, F
    RLF D2, F
    MOVLW 0x3
    ADDWF D0, F
    BTFSS D0, 3
    SUBWF D0, F
    RLF TMP, F
    RLF D0, F
    RLF D2, F
    MOVLW 0x3
    ADDWF D0, F
    BTFSS D0, 3
    SUBWF D0, F
    MOVLW 0x30
    ADDWF D0, F
    BTFSS D0, 7
    SUBWF D0, F
    RLF TMP, F
    RLF D0, F
    RLF D2, F
    MOVLW 0x3
    ADDWF D0, F
    BTFSS D0, 3
    SUBWF D0, F
    MOVLW 0x30
    ADDWF D0, F
    BTFSS D0, 7
    SUBWF D0, F
    RLF TMP, F
    RLF D0, F
    RLF D2, F
    SWAPF D0, W ; unpack
    ANDLW 0x0F
    MOVWF D1
    MOVLW 0x0F
    ANDWF D0, F
    RETURN

DECTOBIN
    BCF STATUS, C
    RLF D1, W ; W = D1 * 2
    MOVWF TMP ; TMP = D1 * 2, W = D1 * 2
    ADDWF TMP, F ; TMP = D1 * 4, W = D1 * 2
    ADDWF TMP, F ; TMP = D1 * 6, W = D1 * 2
    ADDWF TMP, F ; TMP = D1 * 8, W = D1 * 2
    ADDWF TMP, W ; TMP = D1 * 8, W = D1 * 10
    ADDWF D0, W ; W = D1 * 10 + D0
    MOVWF NUM_LO ; that's all for digit 0 and 1
    RLF D2, W ; W = D2 * 2 (addwf above makes carry 0)
    MOVWF TMP ; TMP = D2 * 2
    RLF TMP, W ; W = D2 * 4
    ADDWF NUM_LO, F ; NUM_LO += D2 * 4 (i counted this won't overflow)
    ADDWF TMP, F ; TMP = D2 * 6
    SWAPF TMP, W
    ANDLW 0x0F
    MOVWF NUM_HI
    SWAPF TMP, W
    ANDLW 0xF0
    ADDWF NUM_LO, F
    BTFSC STATUS, C
    INCF NUM_HI, F
    RETURN

INC_NUM ; trashes W
    MOVLW 1
    BTFSS PORTB_SAVE, 6
    MOVLW D'10'
    MOVWF TMP
INC_LOOP
    MOVLW 3
    XORWF NUM_HI, W
    BTFSS STATUS, Z
    GOTO $ + 5
    MOVLW 0xE7
    XORWF NUM_LO, W
    BTFSC STATUS, Z
    RETURN
    INCF NUM_LO, F
    BTFSC STATUS, Z
    INCF NUM_HI, F
    DECFSZ TMP
    GOTO INC_LOOP
    RETURN

DEC_NUM ; trashes W
    MOVLW 1
    BTFSS PORTB_SAVE, 6
    MOVLW D'10'
    MOVWF TMP
DEC_LOOP
    MOVF NUM_HI, F
    BTFSS STATUS, Z
    GOTO $ + 4
    DECF NUM_LO, W
    BTFSC STATUS, Z
    RETURN
    MOVF NUM_LO, F
    BTFSC STATUS, Z
    DECF NUM_HI, F
    DECF NUM_LO, F
    DECFSZ TMP
    GOTO DEC_LOOP
    RETURN

DISP_NUM
    CALL BINTODEC
    ; show time
    MOVF D2, W
    CALL DISP_DIGIT
    MOVF D1, W
    CALL DISP_DIGIT
    MOVF D0, W
    CALL DISP_DIGIT
    RETURN

PULSE
    ; wait until mains input goes negative
    ; TODO perhaps wait for change in either direction to sync instead
    ; this would make debugging ever so slightly less elegant
    BTFSS PORTA, 0
    GOTO $ - 1
    BTFSC PORTA, 0
    GOTO $ - 1
    ; now wait a a while so it is middle a cycle
    ; we depend on the SSR to wait for a zero crossing
    CALL WAIT
    CALL WAIT
    ; prepare
    MOVF NUM_LO, W
    MOVWF PULSE_COUNT_LO
    MOVF NUM_HI, W
    MOVWF PULSE_COUNT_HI
    MOVF PULSE_COUNT_LO, F
    BTFSC STATUS, Z
    DECF PULSE_COUNT_HI, F
    DECF PULSE_COUNT_LO, F
    MOVF PORTA, W
    ; go
    BSF PORTA, 1
PULSE_LOOP
    MOVWF PULSE_TMP ; PORTA in W from last iteration or init
    MOVF PORTA, W
    XORWF PULSE_TMP, F
    BTFSC PULSE_TMP, 0 ; A0 changed
    GOTO PULSE_STEP
    GOTO PULSE_LOOP
PULSE_STEP
    MOVF PULSE_COUNT_HI, F
    BTFSS STATUS, Z
    GOTO $ + 4
    MOVF PULSE_COUNT_LO, F
    BTFSC STATUS, Z
    GOTO PULSE_END
    MOVF PULSE_COUNT_LO, F
    BTFSC STATUS, Z
    DECF PULSE_COUNT_HI, F
    DECF PULSE_COUNT_LO, F
    GOTO PULSE_LOOP
PULSE_END
    CALL WAIT
    BCF PORTA, 1
    RETURN

WAIT
    CLRF PULSE_TMP
    DECFSZ PULSE_TMP, F
    GOTO $ - 1
    CLRF PULSE_TMP
    DECFSZ PULSE_TMP, F
    GOTO $ - 1
    CLRF PULSE_TMP
    DECFSZ PULSE_TMP, F
    GOTO $ - 1
    CLRF PULSE_TMP
    DECFSZ PULSE_TMP, F
    GOTO $ - 1
    RETURN

START
    ; set up serial
    BANKSEL SPBRG
    CLRF SPBRG
    BSF TXSTA, SYNC
    BSF TXSTA, CSRC
    BSF TXSTA, TXEN
    BANKSEL 0
    BSF RCSTA, SPEN
    ; set up portb pull ups etc
    BANKSEL OPTION_REG
    BCF OPTION_REG, 7 ; RBPU
    BCF OPTION_REG, INTEDG  ; interrupt on falling edge
    BANKSEL 0
    BCF INTCON, GIE
    BSF INTCON, RBIE
    BSF INTCON, INTE
    BCF INTCON, RBIF
    BCF INTCON, INTF
    ; GIE enabled later, when we are ready
    
    ; set up porta
    CLRF PORTA
    MOVLW 0x7
    MOVWF CMCON
    BANKSEL TRISA
    BCF TRISA, 1
    BANKSEL 0
    
    ; go
    MOVLW 1
    MOVWF NUM_LO
    CLRF NUM_HI
    CALL DISP_NUM
    CLRF STATE
    BSF INTCON, GIE

LOOP
    BTFSS STATE, 0
    GOTO $ - 1
    CALL PULSE
    BCF STATE, 0
    GOTO LOOP

    END
